#include <cstdio>
#include <cmath>
using namespace std;

int main()
{
    int n;
    scanf("%d", &n);
    printf("%.0f\n", pow(3, n));
    return 0;
    
}
